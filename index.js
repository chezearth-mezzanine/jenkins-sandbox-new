'use strict';

const newModule = require('./new-module');

const a = process.argv[2] ? process.argv[2] : 0;
const b = process.argv[3] ? process.argv[3] : 0;

console.log(newModule.checkDiff(a)(b));
