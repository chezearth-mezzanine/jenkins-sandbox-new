'use strict';


exports.checkDiff = function(a) {
   return function(b) {
    return Math.abs(a - b);
   };
}
